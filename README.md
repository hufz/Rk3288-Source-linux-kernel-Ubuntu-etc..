**Firefly RK3288 RK3399，kernel Ubuntu 相关资料整理**
***
内容极其丰富的：   
[Rockchip Wiki，**官网** Linux Users Guide](http://rockchip.wikidot.com/linux-user-guide#toc2) 
***
其他参考网址：    
[RK3288使用kernel4.4+ubuntu16.04，纯linux下升级包制作](developer.t-firefly.com/thread-12393-1-1.html)     
[Rom烧录](http://wiki.t-firefly.com/index.php/Firefly-RK3288/Flash_image) 
[Firefly Ubuntu系统相关资源汇总](http://dev.t-firefly.com/thread-10874-1-1.html) 	
[Shell编程基础](http://wiki.ubuntu.org.cn/Shell%E7%BC%96%E7%A8%8B%E5%9F%BA%E7%A1%80) 
***
**编译流程及常用命令**
***
编译环境： Ubuntu 16.04 64位    
GCC默认版本：不确定，懒得查   
切换java版本（java/javac1.7.x For Linux kernel 3.1.x; java/c 1.8.x For 4.4.x）   
```
sudo update-alternatives    #//--config java    
sudo update-alternatives    #//--config javac  
```  

RK3288 Linux **内核版本**：4.4.55 来源：[点此链接](https://gitlab.com/TeeFirefly/linux-kernel/tree/gitlab/firefly)     或  https://gitlab.com/TeeFirefly/linux-kernel/tree/gitlab/firefly    
**交叉编译环境**设定：    
 ```
export ARCH=arm 
export CROSS_COMPILE=~/UbuntuDev/toolchain/arm-eabi-4.6/bin/arm-eabi-   
                  #//（自行修改自己交叉工具链所在的对应路径）
#//针对RK3399，应为：
#//  export ARCH=arm64 
#//  export CROSS_COMPILE=/usr/bin/aarch64-linux-gnu-
```   
---
关于  **make menuconfig**     

-  1，```make menuconfig``` 时,若其“标题栏”显示  ".config -** Linux/arm64 **4.4.55 Kernel Configuration" ，此时Save则会保存为arm64相关的.config ，Rk3399内核编译可以直接使用。  
- 2，针对Rk3288内核编译的时候，因为其为32bits ARM v7，故只能```make ARCH=arm menuconfig```，此时“标题栏”应显示  “   .config -  **Linux/arm**  4.4.55 Kernel Configuration   ”(注意粗体部分区别，数字与版本相关，忽略之)，且需首先由 ```make rockchip_linux_defconfig```生成 .config文件 ，然后再通过make ARCH=arm menuconfig 读取.config后再做相应修改。
---
rk3288**烧录内容:**
- 在Firefly Rk3288的平台上，把emmc完全擦除后，烧录如下5个文件即可构成一个完整的ubuntu系统： 
1，Loader；2，Parameter；3，Resource; 4,Boot; 5,rootfs(Ubuntu内核+应用)
- 以上的1，2及5可以由firefly的固件通过rk下载工具解包后获取    
以上的Parameter决定3、4、5在emmc中的位置，其中3,4可以合并为一个文件烧录，此时也需要在2中做相应修改。   
emmc为空的时候，**烧录顺序**为，首先下载 loadxxx.bin parameter(可以分开或同时)，其他下载顺序随意 
 
---
**串口**：```sudo picocom -b 115200 /dev/ttyUSB0 ```  

**烧写**统一**固件** update.img：   
(因为upgrade_tool容易因打字错误导致误输入从而破坏引导，故重命名为rk3288upgradeflashtool）    
   sudo upgrade_tool uf update.img   

烧写分区**镜像**：   
   sudo upgrade_tool di -b /path/to/boot.img   
   sudo upgrade_tool di -k /path/to/kernel.img   
   sudo upgrade_tool di -s /path/to/system.img   
   sudo upgrade_tool di -r /path/to/recovery.img   
   sudo upgrade_tool di -m /path/to/misc.img   
   sudo upgrade_tool di resource /path/to/resource.img   
   sudo upgrade_tool di -p paramater   #烧写 parameter   
   sudo upgrade_tool ul bootloader.bin # 烧写 bootloader   

DI命令：下载单独image镜像到指定扇区,例如升级kernel.img或者system.img都可以直接使用此功能.例如下载kernel.img： DI -k kernel.img parameter //如果之前通过DI下载过parameter，则再下载kernel.img时就可以不用指定最后的parameter参数； 若emmc已经被清空，则下载parameter文件前需先下载load文件   
DB命令：下载boot,在maskrom状态下，可以通过此功能， 让maskrom设备进行Rockusb协议通讯    

如果因 flash 问题导致升级时出错，可以尝试低级格式化、擦除 nand flash：     
    _sudo upgrade_tool ef_     # 擦除   
  EF命令：擦除整个nandflash ，***该参数需要配合load文件使用。***例如：```sudo upgrade_tool ef   -rk32882.37xx.bin```  
  _sudo upgrade_tool lf _   # 低级格式化 
  LF命令：低格保留块后面区域，只有在loader模式下使用   
  CD命令： 选择设备， 当执行的命令有包含设备重启操作时， 需重新选择设备，当改变操作设备时需重新选择   
  SD命令:msc切换到rockusb升级模式。 当切换执行成功后， 需要重新选择设备   
  UF命令:升级完整update.img固件,当执行成功后需要重新选择设备    
  UL命令:升级loader功能，当执行成功后需要重新选择设备    

